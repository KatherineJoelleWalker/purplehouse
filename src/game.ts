
const scene = new Entity()
engine.addEntity(scene)
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)

const floorBaseGrass_01 = new Entity()
engine.addEntity(floorBaseGrass_01)
floorBaseGrass_01.setParent(scene)
const gltfShape = new GLTFShape('models/FloorBaseGrass_01/FloorBaseGrass_01.glb')
floorBaseGrass_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01.addComponentOrReplace(transform_2)

const hauntedHouse5 = new Entity()
engine.addEntity(hauntedHouse5)
hauntedHouse5.setParent(scene)
const gltfShape_2 = new GLTFShape('models/hauntedHouse5.glb')
hauntedHouse5.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
hauntedHouse5.addComponentOrReplace(transform_3)

const door5 = new Entity()
engine.addEntity(door5)
door5.setParent(scene)
const gltfShape_3 = new GLTFShape('models/door5.glb')
door5.addComponentOrReplace(gltfShape_3)
const transform_4 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
door5.addComponentOrReplace(transform_4)
door5.addComponent(new Animator());
// This model has an "Open" animation that when played should happen once
// and then stop moving
door5.getComponent(Animator)
.addClip(new AnimationState("open", { looping: false }));

// When the player clicks on the door, open it!
let isDoorOpen = false;
door5.addComponent(
  new OnClick((): void => {
    // Play the animation
    door5
      .getComponent(Animator)
      .getClip("open")
      .play();

  }))
  
  const speaker = new Entity()
  const clip = new AudioClip('sounds/knock.mp3')
  const source = new AudioSource(clip)
  speaker.addComponent(source)
  source.playing = true

  engine.addEntity(speaker)

const junglePlant_01 = new Entity()
engine.addEntity(junglePlant_01)
junglePlant_01.setParent(scene)
const gltfShape_4 = new GLTFShape('models/JunglePlant_01/JunglePlant_01.glb')
junglePlant_01.addComponentOrReplace(gltfShape_4)
const transform_5 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
junglePlant_01.addComponentOrReplace(transform_5)
